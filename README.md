# Conductor Telegram App
A Telegram bot that recieves messages from telegram and uses API.AI to parse those messages into defined actions in this app. Example of an action can be to look up a song on Spotify and send a request to Conductor server to play that song.

## Setup Instructions
- Create a Telegram bot (https://core.telegram.org/bots).
- Create an account and intents on https://api.ai.
- Open the file imports/configuration/config.js and add the api keys to the services that you want to use.
- Double click on "start.bat" to start the application.
- Write a message to your Telegram bot that matches the intent that you created in api.ai and that message should appear in your conductor-telegram-app.