import { ActionFactory } from './action-factory.js'
import { log } from '../logger/logger.js'
import { Config } from '../configuration/config.js'

var apiai = require("apiai");
var ApiAI = apiai(Config.apiai.apiKey);

var AI = {
    interpret: function(message) {
        log.info("Sending message '{0}' to Api.AI for interpretation.".format(message.text));

        var request = ApiAI.textRequest(message.text, { sessionId: message.from.id });

        request.on('response', Meteor.bindEnvironment(function(event) {
            if (event.result.actionIncomplete) {
                log.warn('Action is incomplete: {0}.'.format(event));
                TelegramBot.send('You didn\'t supply enough information, please try again.', message.chat.id, true);
            } else {
                log.debug('Response received from API.AI.', event);
                AI.triggerAction(event.result.action, event, message);
            }
        }));

        request.on('error', Meteor.bindEnvironment(function(error) {
            log.error('An error occured when sending request to api ai: {0}.'.format(error));
            TelegramBot.send('An error occured when contacting api.ai.', message.chat.id, true);
         }));

        request.end();
    },

    triggerAction: function(actionName, event, message) {
        var action = ActionFactory.getAction(actionName);

        if (action != null) {
            action.trigger(event, message);
        } else {
            var speech = event.result.fulfillment.speech;
            log.info("API.AI response didn\'t match any action, sending the response '{0}' to the user.".format(speech));
            TelegramBot.send(speech, message.chat.id, true);
        }
    }


}

export { AI };