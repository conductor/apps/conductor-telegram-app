import { SpotifyAction } from './actions/spotify-action.js'
import { log } from '../logger/logger.js'

var actions = {
    'spotify.play': SpotifyAction
}

var ActionFactory = {

    getAction: function(actionName) {
        log.debug('Looking up action: {0}'.format(actionName));
        return actions[actionName];
    }

}

export { ActionFactory }