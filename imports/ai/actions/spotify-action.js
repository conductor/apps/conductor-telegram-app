import { Spotify } from '../../services/spotify/spotify.js';
import { log } from '../../logger/logger.js'

var SpotifyAction = {

    trigger: function(event, message) {
        var artist = event.result.parameters['music-artist'];
        var track = event.result.parameters['music-track'];

        log.info("The AI interpreted the message as a Spotify track with the artist '{0}' and the track '{1}'. Searching Spotify for this track...".format(artist, track));

        SpotifyAction.lookupSpotifyTrack(artist, track, message, function(trackUri) {
            log.info("Spotify found the track uri '{0}' for the artist '{1}' and track '{2}':".format(trackUri, artist, track));

            SpotifyAction.changeSpotifyTrack(trackUri, message, function() {
                log.info('Spotify track was successfully changed.');
                TelegramBot.send(event.result.fulfillment.speech, message.chat.id, true);
            });
        });
    },

    lookupSpotifyTrack: function(artist, track, message, callback) {
        Spotify.getTrackURI(artist, track, function(error, trackUri) {
            if (error) {
                log.error('Error recieved from spotify when looking up trackuri for {0} - {1}.'.format(artist, track), error);
            } else if (trackUri) {
                callback(trackUri);
            } else {
                log.info('No spotify track found for ' + artist + ' - ' + track);
                TelegramBot.send("No track found for artist '{0}' and track '{1}'.".format(artist, track), message.chat.id, true);
            }
        });
    },

    changeSpotifyTrack: function(trackUri, message, callback) {
        log.info('Sending request to change spotify track to {0}.'.format(trackUri));

        Conductor.setPropertyValue({ propertyName: 'trackURI' }, trackUri, function(error, result) {
            if (error) {
                log.error('An error occured when sending "set property value" request to conductor: {0}.'.format(error));
                TelegramBot.send('An error occured when sending message to conductor. ' + error.reason, message.chat.id, true);
            } else {
                callback();
            }
        });
    }

}

export { SpotifyAction }