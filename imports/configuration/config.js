// General configuration
Config = {
    apiai: {
        apiKey: 'YOUR_API_AI_API_KEY'
    },
    spotify: {
        clientId: 'YOUR_SPOTIFY_CLIENT_ID',
        clientSecret: 'YOUR_SPOTIFY_CLIENT_SECRET'
    },
    telegram: {
        apiKey: 'YOUR_TELEGRAM_API_KEY'
    },
    conductor: {
        url: 'URL_TO_THE_CONDUCTOR_SERVER',
        apiKey: 'CONDUCTOR_APPLICATION_PRIVATE_KEY'
    },
    logger: {
        file: {
            enable: true,
            location: 'PATH_TO_THE_FOLDER_WHERE_YOU_WANT_TO_STORE_YOUR_LOF_FILES',
            filter: ['ERROR', 'FATAL', 'WARN', 'INFO']
        },
        console: {
            enable: true,
            filter: ['ERROR', 'FATAL', 'WARN', 'INFO', 'DEBUG']
        }
    }
}

export { Config }
