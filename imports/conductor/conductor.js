import { Config } from '../configuration/config.js'

Conductor = require('node-conductor');
Conductor.connect(Config.conductor.url);
Conductor.setApiKey(Config.conductor.apiKey);
Conductor.subscribe(Conductor.Subscription.DEVICES);
Conductor.subscribe(Conductor.Subscription.SET_PROPERTY_VALUE_REQUEST_SUMMARIES);
Conductor.subscribe(Conductor.Subscription.APPLICATION_INVITES);