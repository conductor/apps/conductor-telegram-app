import { log } from '../logger/logger.js';
import { User } from '../user/user.js';
import { AI } from '../ai/ai.js';

TelegramBot.addListener('/start', function(command, username, message) {
    var userId = message.from.id;
    log.info("Received Telegram message '{0}' from '{1} {2}' ({3})".format(message.text, message.from.first_name, message.from.last_name, userId));

    if (User.isRegistered(userId)) {
        TelegramBot.send('Welcome back!', userId);
    } else {
        log.info('User is not registered.');
        User.register(userId);
    }
});

TelegramBot.setCatchAllText(true, function(username, message) {
    log.info("Received Telegram message '{0}' from '{1} {2}' ({3})".format(message.text, message.from.first_name, message.from.last_name, message.from.id));

    if (!User.isRegistered(message.from.id)) {
        User.register(message.from.id);
        return;
    }

    AI.interpret(message);
});

TelegramBot.addListener('/listDevices', function(command) {
    var devices = Conductor.Devices.find().fetch();

    if (devices.length == 0) {
        return 'You haven\'t given access to any devices yet, click here to add devices.';
    }

    var response = 'Here is a list of all devices:\n';

    devices.forEach(device => {
        response += device.name + '\n';
    })

    return response;
});

TelegramBot.addListener('/listDevicesDetailed', function(command) {
    var devices = Conductor.Devices.find().fetch();

    if (devices.length == 0) {
        return 'You haven\'t given access to any devices yet, click here to add devices.';
    }

    var response = 'Here is a list of all devices:\n';

    devices.forEach(device => {
        response += device.name + '\n';

        device.components.forEach(component => {
            response += '\t\t\t\t\t\t' + component.name + '\n';

            component.properties.forEach(property => {
                response += '\t\t\t\t\t\t\t\t\t\t\t\t' + property.name + ' - ' + property.value + '\n';
            })
        })
    })

    return response;
});

TelegramBot.addListener('/listDevicesRaw', function(command) {
    var devices = Conductor.Devices.find().fetch();

    if (devices.length == 0) {
        return 'You haven\'t given access to any devices yet, click here to add devices.';
    }

    var response = 'Here is a list of all devices:\n';

    devices.forEach(device => {
        response += JSON.stringify(device, null, 2) + '\n';
    })

    return response;
});
