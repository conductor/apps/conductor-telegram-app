import { log } from '../logger/logger.js'

var TinyURL = require('tinyurl');

var User = {

    isRegistered: function(userId) {
        var user = Users.findOne({ 'telegram.userId' : userId });
        return user ? user.conductor.publicKey : null;
    },

    register: function(userId) {
        Conductor.createInvite(userId, function(error, result) {
            if (error) {
                log.error('An error occured when sending "createInvite" request to conductor.', error);
                TelegramBot.send('Welcome! We are experience some troubles right now and can\'t create an invitation for you, please try again later.', userId);
            } else {
                log.info('Successfully created a new invite.', result);
                TinyURL.shorten(result.inviteUrl, Meteor.bindEnvironment(function(shortUrl) {
                    TelegramBot.send('Welcome! Please click the following link to accept the invitation ' + shortUrl + '.', userId);
                }));
            }
        });
    }

}

Conductor.Invites.find({}).observe({
    changed: function(invite, oldInvite) {
        log.info('Invite changed.', invite);

        if (invite.userPublicKey && invite.applicationInviteStatus === 'ACCEPTED') {
            Users.insert({
                telegram: {
                    userId: parseInt(invite.externalId)
                },
                conductor: {
                    publicKey: invite.userPublicKey
                }
            });

            TelegramBot.send('You have accepted the invitation!', invite.externalId);
        }
    }
});

export { User }