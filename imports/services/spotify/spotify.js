import { log } from '../../logger/logger.js'
import { Config } from '../../configuration/config.js'

var SpotifyWebApi = require('spotify-web-api-node');

// credentials are optional
var spotifyApi = new SpotifyWebApi({
    clientId: Config.spotify.clientId,
    clientSecret: Config.spotify.clientSecret
});

Spotify = {

    getTrackURI: function(artistName, trackName, callback) {
        spotifyApi.searchTracks('track:' + trackName + ' artist:' + artistName)
            .then(function(data) {
                if (data && data.body && data.body.tracks && data.body.tracks.items && data.body.tracks.items.length > 0 && data.body.tracks.items[0].uri) {
                    callback(null, data.body.tracks.items[0].uri);
                } else {
                    callback(null, null);
                }
            }, function(err) {
                callback(err);
            });
    },

    refreshAccessToken: function() {
        spotifyApi.clientCredentialsGrant()
          .then(function(data) {
            var expiresIn = data.body['expires_in'];
            setTimeout(Spotify.refreshAccessToken, Spotify.toMilliseconds(expiresIn));  // Refresh token when current token expires.

            log.debug('The Spotify access token expires in {0} seconds.'.format(expiresIn));
            log.debug('The Spotify access token is ' + data.body['access_token']);

            // Save the access token so that it's used in future calls
            spotifyApi.setAccessToken(data.body['access_token']);
          }, function(err) {
                log.error('Something went wrong when retrieving an access token', err);
          });
    },

    toMilliseconds: function(seconds) {
        return seconds * 1000;
    }

}

Spotify.refreshAccessToken();

export { Spotify };