import { Logger } from 'meteor/ostrio:logger';
import { LoggerFile } from 'meteor/ostrio:loggerfile';
import { LoggerConsole } from 'meteor/ostrio:loggerconsole';
import { Config } from '../configuration/config.js'

const log = new Logger();

// Initialize LoggerConsole:
new LoggerConsole(log, {
    format: function(options) {
        return formatMessage(options.level, options.time, options.message, options.data, options.userId);
    }
}).enable({
    enable: Config.logger.console.enable,
    filter: Config.logger.console.filter
});

// Initialize LoggerFile:
new LoggerFile(log, {
    fileNameFormat: function(time) {
        return time.getDate() + "-" + (time.getMonth() + 1) + "-" + time.getFullYear() + ".log"; // Create log-files daily
    },
    format: function(time, level, message, data, userId) {
        return formatMessage(level, time, message, data, userId);
    },
    path: Config.logger.fileLocation // Use absolute storage path
}).enable({
    enable: Config.logger.file.enable,
    filter: Config.logger.file.filter
});

function formatMessage(level, time, message, data, userId) {
    return '[' + level + '] ' +
        moment(time).format('YYYYMMDD-hh:mm:ss') +
        ' - ' +
        message + '\r\n' +
        (!isEmpty(data) ? toString(data) + '\r\n' : '') +
        ((userId != null) ? 'User: ' + userId + '\r\n' : '');
}

function toString(value) {
    if (value == null) {
        return null;
    } else if (isPrimitive(value)) {
        return value;
    } else {
        return JSON.stringify(value, null, 4);
    }
}

function isEmpty(obj) {
    if (obj == null) {
        return true;
    }

    return Object.keys(obj).length === 0 && obj.constructor === Object;
}

function isPrimitive(test) {
    return (test !== Object(test));
};

export { log }