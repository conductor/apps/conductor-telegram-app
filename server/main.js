import { Meteor } from 'meteor/meteor';
import '../imports/utils/string.js';
import '../imports/configuration/config.js';
import '../imports/logger/logger.js';
import '../imports/conductor/conductor.js';
import '../imports/collections/collections.js';
import '../imports/services/spotify/spotify.js';
import '../imports/telegram/routes.js';
import '../imports/telegram/setup.js';
